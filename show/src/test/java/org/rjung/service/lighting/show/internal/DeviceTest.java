package org.rjung.service.lighting.show.internal;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.rjung.service.lighting.configuration.Channel;
import org.rjung.service.lighting.configuration.DeviceConfiguration;
import org.rjung.service.lighting.configuration.FixtureConfiguration;
import org.rjung.service.lighting.shared.ConfigurationException;

import java.util.List;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

class DeviceTest {

    @Test
    void throwsExceptionWhenCreatedWithoudAvailableFixture() {
        DeviceConfiguration configuration = new DeviceConfiguration("name", "fixture", 123);
        Map<String, Fixture> fixtureMap = Map.of();

        var exception = assertThrows(ConfigurationException.class, () -> new Device(configuration, fixtureMap));

        assertThat(exception.getMessage(), Matchers.is("Cannot find fixture 'fixture'"));
    }

    @Test
    void assignsPropertiesWhenCreated() {
        int startAddress = 123;
        DeviceConfiguration configuration = new DeviceConfiguration("name", "fixture", startAddress);
        Fixture fixture = new Fixture(new FixtureConfiguration("fixture", List.of(Channel.MASTER)));
        Map<String, Fixture> fixtureMap = Map.of("fixture", fixture);

        var sut = new Device(configuration, fixtureMap);

        assertThat(sut, allOf(
                hasProperty("fixture", is(fixture)),
                hasProperty("startAddress", is(startAddress))));
    }

}