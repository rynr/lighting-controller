package org.rjung.service.lighting.show.internal;

import org.junit.jupiter.api.Test;
import org.rjung.service.lighting.configuration.Channel;
import org.rjung.service.lighting.configuration.FixtureConfiguration;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.is;

class FixtureTest {

    @Test
    void assignsPropertiesWhenCreated() {
        List<Channel> channels = List.of(Channel.MASTER);
        FixtureConfiguration configuration = new FixtureConfiguration("name", channels);

        var sut = new Fixture(configuration);

        assertThat(sut, hasProperty("channels", is(channels)));
    }

}