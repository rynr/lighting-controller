package org.rjung.service.lighting.show.internal;

import org.hamcrest.Matcher;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.rjung.service.lighting.configuration.SceneConfiguration;
import org.rjung.service.lighting.configuration.SectionConfiguration;
import org.rjung.service.lighting.configuration.ValueConfiguration;
import org.rjung.service.lighting.shared.ConfigurationException;
import org.rjung.service.lighting.shared.Dmx;
import org.rjung.service.lighting.shared.DmxContext;

import java.time.Instant;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class SectionTest {

    @Mock
    private Scene scene;

    public static Stream<Arguments> throwsExceptionsOnParameters() {
        return Stream.of(
                Arguments.of(
                        new SectionConfiguration("name", 0, Set.of("scene"), Set.of()),
                        Map.of(),
                        "Cannot find all scenes for section 'name' ('scene')"),
                Arguments.of(
                        new SectionConfiguration("name", 0, Set.of(), Set.of("scene")),
                        Map.of(),
                        "Cannot find all scenes for section 'name' ('scene')"),
                Arguments.of(
                        new SectionConfiguration("name", 0, Set.of("scene"), Set.of()),
                        Map.of("other", new Scene(new SceneConfiguration("name", List.of(new ValueConfiguration())), Map.of())),
                        "Cannot find all scenes for section 'name' ('scene')"),
                Arguments.of(
                        new SectionConfiguration("name", 0, Set.of(), Set.of("scene")),
                        Map.of("other", new Scene(new SceneConfiguration("name", List.of(new ValueConfiguration())), Map.of())),
                        "Cannot find all scenes for section 'name' ('scene')")
        );
    }

    public static Stream<Arguments> assignsPropertiesToSection() {
        return Stream.of(
                Arguments.of(
                        new SectionConfiguration("name", 0, Set.of("scene"), Set.of("scene")),
                        Map.of("scene", new Scene(new SceneConfiguration("scene", List.of(new ValueConfiguration())), Map.of())),
                        allOf(
                                hasProperty("name", is("name"))
                                // hasProperty("bpm", is((short) 0)),
                                // hasProperty("scenes", contains(
                                //         hasProperty("name", is("scene"))
                                // )),
                                // hasProperty("effectScenes", contains(
                                //         hasProperty("name", is("scene"))
                                // ))
                        )
                )
        );
    }

    @MethodSource
    @ParameterizedTest
    void throwsExceptionsOnParameters(SectionConfiguration configuration, Map<String, Scene> sceneMap, String expectedErrorMessage) {
        var exception = assertThrows(ConfigurationException.class, () -> new Section(configuration, sceneMap));

        assertThat(exception.getMessage(), is(expectedErrorMessage));
    }

    @MethodSource
    @ParameterizedTest
    void assignsPropertiesToSection(SectionConfiguration configuration, Map<String, Scene> sceneMap, Matcher<Section> matcher) {
        var result = new Section(configuration, sceneMap);

        assertThat(result, matcher);
    }

    @Test
    void shouldDefaultBpmTo120() {
        var sut = new Section(new SectionConfiguration("section", null, Set.of(), Set.of()), Map.of());

        assertThat(sut.getBpm(), is((short) 120));
    }

    @Test
    void shouldRetrieveDmxFromScene() {
        var sceneCode = "scene";
        var expectedDmx = new Dmx();
        var now = Instant.now();
        var context = new DmxContext((short) 120, now);
        expectedDmx.setInt8(123, 123);
        when(scene.getDmx(context)).thenReturn(expectedDmx);

        var sut = new Section(new SectionConfiguration("section", 120, Set.of(sceneCode), Set.of()), Map.of(sceneCode, scene));

        assertThat(sut.getDmx(now, false), is(expectedDmx));
    }

    @Test
    void shouldRetrieveDmxFromEffectScene() {
        var sceneCode = "scene";
        var expectedDmx = new Dmx();
        var now = Instant.now();
        var context = new DmxContext((short) 120, now);
        expectedDmx.setInt8(123, 123);
        when(scene.getDmx(context)).thenReturn(expectedDmx);

        var sut = new Section(new SectionConfiguration("section", 120, Set.of(), Set.of(sceneCode)), Map.of(sceneCode, scene));

        assertThat(sut.getDmx(now, true), is(expectedDmx));
    }

}