package org.rjung.service.lighting.show;

import lombok.Getter;
import org.junit.jupiter.api.Test;
import org.rjung.service.lighting.configuration.ShowConfiguration;
import org.rjung.service.lighting.shared.Dmx;

import java.time.Instant;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.closeTo;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

class ShowTest {

    @Test
    void canBuildShow() {
        assertDoesNotThrow(() -> Show.build(new ShowConfiguration()));
    }

    @Test
    void callsGetDmxWithCurrentInstant() {
        var sut = new ShowImpl();
        var result = sut.getDmx();

        assertThat((double) sut.getCalledInstant().getEpochSecond(), closeTo(Instant.now().getEpochSecond(), 1D));
        assertThat(result, is(ShowImpl.EXAMPLE_DMX));
    }

    @Getter
    private static class ShowImpl implements Show {

        public static final Dmx EXAMPLE_DMX = new Dmx(new byte[]{0, 1, 2, 3, 4});
        private Instant calledInstant = null;

        @Override
        public String getSection() {
            return "section";
        }

        @Override
        public void prevSection() {

        }

        @Override
        public void nextSection() {

        }

        @Override
        public void setEffect(boolean effectEnabled) {

        }

        @Override
        public Dmx getDmx(Instant instant) {
            this.calledInstant = instant;
            return EXAMPLE_DMX;
        }
    }
}