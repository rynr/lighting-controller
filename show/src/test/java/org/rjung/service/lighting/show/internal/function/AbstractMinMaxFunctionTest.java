package org.rjung.service.lighting.show.internal.function;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.rjung.service.lighting.configuration.Channel;
import org.rjung.service.lighting.configuration.ValueConfiguration;
import org.rjung.service.lighting.shared.ConfigurationException;
import org.rjung.service.lighting.shared.DmxContext;

import java.time.Instant;
import java.util.Set;
import java.util.stream.Stream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.either;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertThrows;

class AbstractMinMaxFunctionTest {

    public static Stream<Arguments> shouldSetOffset() {
        return Stream.of(
                Arguments.of(0, 0, 0, (short) 0),
                Arguments.of(10, 0, 0, (short) 10),
                Arguments.of(360, 0, 0, (short) 0),
                Arguments.of(370, 0, 0, (short) 10),
                Arguments.of(0, 60, 0, (short) 0),
                Arguments.of(0, 60, 1, (short) 60),
                Arguments.of(0, 60, 6, (short) 0),
                Arguments.of(0, 60, 7, (short) 60),
                Arguments.of(10, 10, 1, (short) 20),
                Arguments.of(10, 60, 6, (short) 10),
                Arguments.of(null, 0, 0, (short) 0)
        );
    }

    @Test
    void shouldChoseZeroToMinIfNoValueGiven() {
        var function = new TestMinMaxFunction(new ValueConfiguration(Set.of(), Set.of(), 127, null, ValueConfiguration.Type.STATIC, 1d, 0, 0), 0);

        assertThat(function.min, is(0D));
        assertThat(function.max, is(127.999D)); // 0.999 is added for better range
    }

    @Test
    void shouldChosemaxToValueIfNoValueGiven() {
        var function = new TestMinMaxFunction(new ValueConfiguration(Set.of(), Set.of(), null, 45, ValueConfiguration.Type.STATIC, 1d, 0, 0), 0);

        assertThat(function.min, is(45D));
        assertThat(function.max, is(255.999D)); // 0.999 is added for better range
    }

    @MethodSource
    @ParameterizedTest
    void shouldSetOffset(Integer offset, Integer phase, int deviceNr, short expectedOffset) {
        var function = new TestMinMaxFunction(new ValueConfiguration(Set.of(), Set.of(), null, null, ValueConfiguration.Type.STATIC, 1d, offset, phase), deviceNr);

        assertThat(function.offset, is(expectedOffset));
    }

    @ParameterizedTest
    @ValueSource(doubles = {-9999, -1, 0.0001, 4.0001, 9999})
    void shouldNotAcceptInvalidRate(Double rate) {
        ValueConfiguration configuration = new ValueConfiguration(Set.of(), Set.of(), 0, null, ValueConfiguration.Type.STATIC, rate, 0, 0);

        var exception = assertThrows(ConfigurationException.class, () -> new TestMinMaxFunction(configuration, 0));

        assertThat(exception.getMessage(), is("Rate needs to be within 0.001 and 4"));
    }

    @ParameterizedTest
    @ValueSource(ints = {-9999, -1, 256, 9999})
    void shouldNotAcceptInvalidValue(Integer value) {
        ValueConfiguration configuration = new ValueConfiguration(Set.of(), Set.of(), value, null, ValueConfiguration.Type.STATIC, 1d, 0, 0);

        var exception = assertThrows(ConfigurationException.class, () -> new TestMinMaxFunction(configuration, 0));

        assertThat(exception.getMessage(),
                either(is("Values need to be >= 0"))
                        .or(is("Values need to be <= 255")));
    }

    @CsvSource({"-1,0", "-2,-4", "-1,256", "0,256", "256,999"})
    @ParameterizedTest
    void shouldNotAcceptInvalidRange(Integer min, Integer max) {
        ValueConfiguration configuration = new ValueConfiguration(Set.of(), Set.of(), max, min, ValueConfiguration.Type.STATIC, 1d, 0, 0);

        var exception = assertThrows(ConfigurationException.class, () -> new TestMinMaxFunction(configuration, 0));

        assertThat(exception.getMessage(),
                either(is("Values need to be >= 0"))
                        .or(is("Values need to be <= 255")));
    }

    @CsvSource({"0,0", "0,1", "1,127", "0,255", "255,255"})
    @ParameterizedTest
    void shouldForBpmZeroAlwaysReturnMax(Integer min, Integer max) {
        ValueConfiguration configuration = new ValueConfiguration(Set.of(), Set.of(), max, min, ValueConfiguration.Type.STATIC, 1d, 0, 0);
        DmxContext context = new DmxContext((short) 0, Instant.now());
        var function = new TestMinMaxFunction(configuration, 0);

        var result = function.getChannelValues(context);

        assertThat(result, is(max.byteValue()));
    }

    @Test
    void shouldPerformRate() {
        DmxContext context = new DmxContext((short) 120, Instant.ofEpochMilli(20));
        ValueConfiguration slow = new ValueConfiguration(Set.of(), Set.of(Channel.MASTER), null, null, ValueConfiguration.Type.STATIC, 0.5d, 0, 0);
        ValueConfiguration normal = new ValueConfiguration(Set.of(), Set.of(Channel.MASTER), null, null, ValueConfiguration.Type.STATIC, 1d, 0, 0);
        ValueConfiguration fast = new ValueConfiguration(Set.of(), Set.of(Channel.MASTER), null, null, ValueConfiguration.Type.STATIC, 2d, 0, 0);

        var functionSlow = new TestMinMaxFunction(slow, 0);
        var functionNormal = new TestMinMaxFunction(normal, 0);
        var functionFast = new TestMinMaxFunction(fast, 0);

        var resultSlow = functionSlow.getChannelValues(context);
        var resultNormal = functionNormal.getChannelValues(context);
        var resultFast = functionFast.getChannelValues(context);

        assertThat(resultSlow, is((byte) 5));
        assertThat(resultNormal, is((byte) 10));
        assertThat(resultFast, is((byte) 20));
    }

    private static final class TestMinMaxFunction extends AbstractMinMaxFunction {

        private TestMinMaxFunction(ValueConfiguration configuration, int deviceNr) {
            super(configuration, deviceNr);
        }

        @Override
        protected byte function(double input) {
            return (byte) (255.9999 * input);
        }
    }


}