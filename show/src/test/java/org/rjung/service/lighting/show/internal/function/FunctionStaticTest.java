package org.rjung.service.lighting.show.internal.function;

import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.rjung.service.lighting.configuration.ValueConfiguration;
import org.rjung.service.lighting.shared.DmxContext;

import java.util.Set;
import java.util.stream.Stream;

class FunctionStaticTest {

    public static Stream<Arguments> providesTheGiven() {
        return Stream.of(
                Arguments.of(255),
                Arguments.of(123),
                Arguments.of(0)
        );
    }

    @MethodSource
    @ParameterizedTest
    void providesTheGiven(Integer value) {
        var sut = new FunctionStatic(new ValueConfiguration(Set.of(), Set.of(), value, 0, ValueConfiguration.Type.STATIC, 1d, 0, 0));
        DmxContext context = new DmxContext(null, null);

        MatcherAssert.assertThat(sut.getChannelValues(context), Matchers.is(value.byteValue()));
    }
}