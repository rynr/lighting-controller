package org.rjung.service.lighting.show.internal.function;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.rjung.service.lighting.configuration.ValueConfiguration;
import org.rjung.service.lighting.shared.DmxContext;

import java.time.Instant;
import java.util.Set;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

class FunctionSquareTest {

    private FunctionSquare sut;

    @BeforeEach
    public void setup() {
        sut = new FunctionSquare(
                new ValueConfiguration(
                        Set.of(),
                        Set.of(),
                        255,
                        0,
                        ValueConfiguration.Type.SQUARE,
                        1d,
                        0,
                        0),
                0);
    }

    @ParameterizedTest
    @CsvSource({"0,0", "749,0", "750,-1", "1499,-1", "1500,0"})
    void shouldRetrieveValuesByTime(long epoch, byte expectedResult) {
        var context = new DmxContext((short) 120, Instant.ofEpochMilli(epoch));

        var result = sut.getChannelValues(context);

        assertThat(result, is(expectedResult));
    }


}