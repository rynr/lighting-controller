package org.rjung.service.lighting.show.internal.function;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.rjung.service.lighting.configuration.ValueConfiguration;
import org.rjung.service.lighting.shared.DmxContext;

import java.time.Instant;
import java.util.Set;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

class FunctionSineTest {

    private FunctionSine sut;

    @BeforeEach
    public void setup() {
        sut = new FunctionSine(
                new ValueConfiguration(
                        Set.of(),
                        Set.of(),
                        null,
                        null,
                        ValueConfiguration.Type.SINE,
                        1d, 0, 0),
                0);
    }

    @ParameterizedTest
    @CsvSource({"0,127", "125,-127", "250,127", "375,0", "500,127"})
    void shouldRenderKnownValues(long epoch, byte expectedResult) {
        var context = new DmxContext((short) 120, Instant.ofEpochMilli(epoch));

        var result = sut.getChannelValues(context);

        assertThat("Value at timestamp " + epoch + " should be " + expectedResult, result, is(result));
    }

}