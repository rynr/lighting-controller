package org.rjung.service.lighting.show.internal;

import org.junit.jupiter.api.Test;
import org.rjung.service.lighting.configuration.*;
import org.rjung.service.lighting.shared.ConfigurationException;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

class SceneTest {

    @Test
    void shouldBuildFunctionsForChannels() {
        var fixtureConfiguration = new FixtureConfiguration("fixture", List.of(
                Channel.RED, Channel.GREEN, Channel.BLUE,
                Channel.RED, Channel.GREEN, Channel.BLUE));
        var fixtureMap = Map.of("fixture", new Fixture(fixtureConfiguration));
        var deviceConfiguration = new DeviceConfiguration("device", "fixture", 0);
        var device = new Device(deviceConfiguration, fixtureMap);
        var deviceMap = Map.of("device", device);
        var sceneConfiguration = new SceneConfiguration("scene", List.of(
                new ValueConfiguration(
                        Set.of("device"),
                        Set.of(Channel.RED),
                        127,
                        null,
                        ValueConfiguration.Type.STATIC,
                        null,
                        null,
                        null)));

        var scene = new Scene(sceneConfiguration, deviceMap);

        // Yes, I test the internals, which is a bad practice, but I lack of a better idea.
        Map<Integer, Function> functionLookup = Collections.unmodifiableMap((Map<Integer, Function>) ReflectionTestUtils.getField(scene, "functionLookup"));

        assertThat(functionLookup, allOf(
                hasEntry(is(0), any(Function.class)), // First RED
                hasEntry(is(3), any(Function.class)))); // Second RED
    }

    @Test
    void throwsOnMissingDevice() {
        var sceneConfiguration = new SceneConfiguration("scene", List.of(
                new ValueConfiguration(
                        Set.of("device"),
                        Set.of(Channel.RED),
                        127,
                        null,
                        ValueConfiguration.Type.STATIC,
                        null,
                        null,
                        null)));
        Map<String, Device> emptyDeviceMap = Map.of();

        var exception = assertThrows(ConfigurationException.class, () -> new Scene(sceneConfiguration, emptyDeviceMap));

        assertThat(exception.getMessage(), is("Cannot find device 'device'"));
    }

    @Test
    void shouldValidateDuplicateChannelAssignments() {
        var fixtureConfiguration = new FixtureConfiguration("fixture", List.of(Channel.RED, Channel.GREEN, Channel.BLUE));
        var fixtureMap = Map.of("fixture", new Fixture(fixtureConfiguration));
        var deviceConfiguration = new DeviceConfiguration("device", "fixture", 0);
        var device = new Device(deviceConfiguration, fixtureMap);
        var deviceMap = Map.of("device", device);
        // Duplicate assignment of RED:
        var sceneConfiguration = new SceneConfiguration("scene", List.of(
                new ValueConfiguration(Set.of("device"), Set.of(Channel.RED), 0, null, ValueConfiguration.Type.STATIC, null, null, null),
                new ValueConfiguration(Set.of("device"), Set.of(Channel.RED), 255, null, ValueConfiguration.Type.STATIC, null, null, null)));

        var exception = assertThrows(ConfigurationException.class, () -> new Scene(sceneConfiguration, deviceMap));

        assertThat(exception.getMessage(), is("Duplicate assignment of channel 'Red' on device 'device' for scene 'scene'"));
    }

}