package org.rjung.service.lighting.show.internal;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.rjung.service.lighting.configuration.*;
import org.rjung.service.lighting.shared.Dmx;

import java.time.Instant;
import java.util.List;
import java.util.Set;
import java.util.stream.IntStream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

class ShowImplTest {

    private ShowImpl sut;

    @BeforeEach
    public void setup() {
        sut = new ShowImpl(new ShowConfiguration(
                Set.of(
                        new FixtureConfiguration("RGB", List.of(Channel.RED, Channel.GREEN, Channel.BLUE)),
                        new FixtureConfiguration("RGBM", List.of(Channel.RED, Channel.GREEN, Channel.BLUE, Channel.MASTER)),
                        new FixtureConfiguration("PIX", List.of(
                                Channel.RED, Channel.GREEN, Channel.BLUE,
                                Channel.RED, Channel.GREEN, Channel.BLUE,
                                Channel.RED, Channel.GREEN, Channel.BLUE,
                                Channel.RED, Channel.GREEN, Channel.BLUE)),
                        new FixtureConfiguration("PIXM", List.of(
                                Channel.RED, Channel.GREEN, Channel.BLUE,
                                Channel.RED, Channel.GREEN, Channel.BLUE,
                                Channel.RED, Channel.GREEN, Channel.BLUE,
                                Channel.RED, Channel.GREEN, Channel.BLUE,
                                Channel.MASTER))),
                Set.of(
                        new DeviceConfiguration("rgb", "RGB", 0),
                        new DeviceConfiguration("rgbm", "RGBM", 3),
                        new DeviceConfiguration("pix", "PIX", 7),
                        new DeviceConfiguration("pixm", "PIXM", 19)),
                Set.of(
                        new SceneConfiguration("black", List.of(
                                new ValueConfiguration(
                                        Set.of("rgb", "rgbm", "pix", "pixm"),
                                        Set.of(Channel.RED, Channel.GREEN, Channel.BLUE, Channel.MASTER),
                                        0,
                                        null,
                                        ValueConfiguration.Type.STATIC,
                                        0D,
                                        0,
                                        1))),
                        new SceneConfiguration("med", List.of(
                                new ValueConfiguration(
                                        Set.of("rgb", "pix"),
                                        Set.of(Channel.RED, Channel.GREEN, Channel.BLUE),
                                        127,
                                        null,
                                        ValueConfiguration.Type.STATIC,
                                        0D,
                                        0,
                                        1),
                                new ValueConfiguration(
                                        Set.of("rgbm", "pixm"),
                                        Set.of(Channel.RED, Channel.GREEN, Channel.BLUE),
                                        255,
                                        null,
                                        ValueConfiguration.Type.STATIC,
                                        0D,
                                        0,
                                        1),
                                new ValueConfiguration(
                                        Set.of("rgbm", "pixm"),
                                        Set.of(Channel.MASTER),
                                        127,
                                        null,
                                        ValueConfiguration.Type.STATIC,
                                        0D,
                                        0,
                                        1))),
                        new SceneConfiguration("lux", List.of(
                                new ValueConfiguration(
                                        Set.of("rgb", "rgbm", "pix", "pixm"),
                                        Set.of(Channel.RED, Channel.GREEN, Channel.BLUE, Channel.MASTER),
                                        255,
                                        null,
                                        ValueConfiguration.Type.STATIC,
                                        0D,
                                        0,
                                        1)))),
                List.of(
                        new SectionConfiguration(
                                "blackout",
                                null,
                                Set.of("black"),
                                Set.of()),
                        new SectionConfiguration(
                                "medium",
                                null,
                                Set.of("med"),
                                Set.of()),
                        new SectionConfiguration(
                                "lux",
                                null,
                                Set.of("lux"),
                                Set.of()))));
    }

    @Test
    void shouldDefaultToInitialSection() {
        assertThat(sut.getSection(), is("blackout"));
    }

    @Test
    void shouldNotNavigateToSectionPriorFirst() {
        IntStream.of(5).forEach(i -> {
            sut.prevSection();
            assertThat(sut.getSection(), is("blackout"));
        });
    }

    @Test
    void shouldNavigate() {
        assertThat(sut.getSection(), is("blackout"));
        sut.nextSection(); // blackout -> medium
        assertThat(sut.getSection(), is("medium"));
        sut.nextSection(); // medium -> lux
        assertThat(sut.getSection(), is("lux"));
        sut.nextSection(); // lux -> lux
        assertThat(sut.getSection(), is("lux"));
        sut.prevSection(); // lux -> medium
        assertThat(sut.getSection(), is("medium"));
        sut.prevSection(); // medium -> blackout
        assertThat(sut.getSection(), is("blackout"));
    }

    @Test
    void shouldNotNavigateToSectionPastLast() {
        sut.nextSection(); // blackout -> mid
        sut.nextSection(); // mid -> lux
        IntStream.of(5).forEach(i -> {
            sut.nextSection();
            assertThat(sut.getSection(), is("lux"));
        });
    }

    @Test
        // This should be an integration test
    void rendersSections() {

        assertThat(sut.getDmx(Instant.now()), is(new Dmx(new byte[]{
                0, 0, 0, // rgb
                0, 0, 0, 0, // rgbm
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // pix
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}))); // pixm
        sut.nextSection();
        assertThat(sut.getDmx(Instant.now()), is(new Dmx(new byte[]{
                127, 127, 127, // rgb
                -1, -1, -1, 127, // rgbm
                127, 127, 127, 127, 127, 127, 127, 127, 127, 127, 127, 127, // pix
                -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 127}))); // pixm
        sut.nextSection();
        assertThat(sut.getDmx(Instant.now()), is(new Dmx(new byte[]{
                -1, -1, -1, // rgb
                -1, -1, -1, -1, // rgbm
                -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, // pix
                -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1}))); // pixm
    }

}