package org.rjung.service.lighting.show.internal;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import org.rjung.service.lighting.configuration.Channel;
import org.rjung.service.lighting.configuration.FixtureConfiguration;

import java.util.List;

@EqualsAndHashCode
@Getter
@ToString
public class Fixture {

    private final List<Channel> channels;

    public Fixture(FixtureConfiguration configuration) {
        channels = configuration.getChannels();
    }

}
