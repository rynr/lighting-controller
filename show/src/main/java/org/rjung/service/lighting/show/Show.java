package org.rjung.service.lighting.show;

import org.rjung.service.lighting.configuration.ShowConfiguration;
import org.rjung.service.lighting.shared.Dmx;
import org.rjung.service.lighting.show.internal.ShowImpl;

import java.time.Instant;

/**
 * Public interface of a {@link Show}.
 */
public interface Show {

    static Show build(ShowConfiguration configuration) {
        return new ShowImpl(configuration);
    }

    /**
     * Retrieve the current active Section.
     *
     * @return Name of the current active Section.
     */
    String getSection();

    /**
     * Switch the show to the previous Section.
     */
    void prevSection();

    /**
     * Switch the show to the next Section. To prevent concurrency issues, the code if the current section needs to be
     * added as parameter. Only if the parameter matches, the previous section is chosen.
     */
    void nextSection();

    /**
     * Enable or disable the effect.
     * Any Section can have 1 effect, which means some scenes are enabled or disabled while the effect is active (think
     * about some blinders for the audience).
     *
     * @param effectEnabled Mark effect on (true) or off (false).
     */
    void setEffect(boolean effectEnabled);

    /**
     * Retrieve all the current DMX addresses and values to be sent.
     *
     * @return Array of all DMX values.
     */
    default Dmx getDmx() {
        return getDmx(Instant.now());
    }

    /**
     * Retrieve all the current DMX addresses and values to be sent.
     *
     * @param instant The instant, the values are calculated for.
     * @return Array of all DMX values.
     */
    Dmx getDmx(Instant instant);

}
