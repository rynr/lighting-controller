package org.rjung.service.lighting.show.internal.function;

import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.rjung.service.lighting.configuration.ValueConfiguration;
import org.rjung.service.lighting.shared.DmxContext;
import org.rjung.service.lighting.show.internal.Function;

@EqualsAndHashCode
@ToString
public class FunctionStatic implements Function {

    private final byte value;

    public FunctionStatic(ValueConfiguration configuration) {
        value = configuration.getValue().byteValue();
    }

    @Override
    public Byte getChannelValues(DmxContext context) {
        return value;
    }

}
