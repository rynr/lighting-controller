package org.rjung.service.lighting.show.internal;

import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import org.rjung.service.lighting.configuration.SectionConfiguration;
import org.rjung.service.lighting.shared.ConfigurationException;
import org.rjung.service.lighting.shared.Dmx;
import org.rjung.service.lighting.shared.DmxContext;

import java.time.Instant;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@EqualsAndHashCode
@ToString
public class Section {

    @Getter
    private final String name;
    @Getter(AccessLevel.PACKAGE)
    private final Short bpm;
    @Getter(AccessLevel.PACKAGE)
    private final Set<Scene> scenes;
    @Getter(AccessLevel.PACKAGE)
    private final Set<Scene> effectScenes;

    public Section(SectionConfiguration configuration, Map<String, Scene> sceneMap) {
        if (!sceneMap.keySet().containsAll(configuration.getScenes())) {
            throw new ConfigurationException("Cannot find all scenes for section '" + configuration.getName() + "' (" + configuration.getScenes().stream().map(d -> "'" + d + "'").collect(Collectors.joining(", ")) + ")");
        }
        if (!sceneMap.keySet().containsAll(configuration.getEffect())) {
            throw new ConfigurationException("Cannot find all scenes for section '" + configuration.getName() + "' (" + configuration.getEffect().stream().map(d -> "'" + d + "'").collect(Collectors.joining(", ")) + ")");
        }

        name = configuration.getName();
        bpm = configuration.getBpm() != null ? configuration.getBpm().shortValue() : 120;
        scenes = configuration.getScenes().stream().map(sceneMap::get).collect(Collectors.toSet());
        effectScenes = configuration.getEffect().stream().map(sceneMap::get).collect(Collectors.toSet());
    }

    public Dmx getDmx(Instant instant, boolean effectOn) {
        var context = new DmxContext(bpm, instant);
        var selectedScenes = effectOn ? effectScenes : scenes;
        return Dmx.join(selectedScenes.stream().map(s -> s.getDmx(context)).toArray(Dmx[]::new));
    }

}
