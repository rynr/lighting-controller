package org.rjung.service.lighting.show.internal;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import org.rjung.service.lighting.configuration.DeviceConfiguration;
import org.rjung.service.lighting.shared.ConfigurationException;

import java.util.Map;

@EqualsAndHashCode
@Getter
@ToString
public class Device {

    private final Fixture fixture;
    private final int startAddress;

    public Device(DeviceConfiguration configuration, Map<String, Fixture> fixtures) {
        if (!fixtures.containsKey(configuration.getFixture())) {
            throw new ConfigurationException("Cannot find fixture '" + configuration.getFixture() + "'");
        }

        fixture = fixtures.get(configuration.getFixture());
        startAddress = configuration.getStartAddress();
    }

}
