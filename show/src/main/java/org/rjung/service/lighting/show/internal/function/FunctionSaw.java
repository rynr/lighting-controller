package org.rjung.service.lighting.show.internal.function;

import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.rjung.service.lighting.configuration.ValueConfiguration;

@EqualsAndHashCode(callSuper = true)
@ToString
public class FunctionSaw extends AbstractMinMaxFunction {

    public FunctionSaw(ValueConfiguration configuration, Integer deviceNr) {
        super(configuration, deviceNr);
    }

    @Override
    protected byte function(double input) {
        return (byte) (min + (input * (max - min)));
    }

}
