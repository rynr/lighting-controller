package org.rjung.service.lighting.show.internal.function;

import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.rjung.service.lighting.configuration.ValueConfiguration;

@EqualsAndHashCode(callSuper = true)
@ToString
public class FunctionSine extends AbstractMinMaxFunction {

    public FunctionSine(ValueConfiguration configuration, Integer deviceNr) {
        super(configuration, deviceNr);
    }

    @Override
    protected byte function(double input) {
        return (byte) (min + ((1 + Math.sin(input * 2 * Math.PI)) * (max - min) / 2));
    }

}
