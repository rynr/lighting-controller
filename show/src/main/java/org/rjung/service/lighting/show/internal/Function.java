package org.rjung.service.lighting.show.internal;

import org.rjung.service.lighting.shared.DmxContext;

public interface Function {

    Byte getChannelValues(DmxContext context);

}
