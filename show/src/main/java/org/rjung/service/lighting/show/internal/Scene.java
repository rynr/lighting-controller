package org.rjung.service.lighting.show.internal;

import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.rjung.service.lighting.configuration.Channel;
import org.rjung.service.lighting.configuration.SceneConfiguration;
import org.rjung.service.lighting.configuration.ValueConfiguration;
import org.rjung.service.lighting.shared.ConfigurationException;
import org.rjung.service.lighting.shared.Dmx;
import org.rjung.service.lighting.shared.DmxContext;
import org.rjung.service.lighting.show.internal.function.FunctionSaw;
import org.rjung.service.lighting.show.internal.function.FunctionSine;
import org.rjung.service.lighting.show.internal.function.FunctionSquare;
import org.rjung.service.lighting.show.internal.function.FunctionStatic;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@EqualsAndHashCode
@ToString(of = {"name"})
public class Scene {

    private final String name;
    private final Map<Integer, Function> functionLookup;

    public Scene(SceneConfiguration configuration, Map<String, Device> deviceMap) {
        this.functionLookup = new HashMap<>();
        this.name = configuration.getName();

        var deviceCount = new AtomicInteger(0);

        configuration.getValues().forEach(entry -> entry.getDevices().forEach(deviceName -> {
            var deviceNumber = deviceCount.getAndIncrement();
            var device = lookupDevice(deviceMap, deviceName);
            var channels = device.getFixture().getChannels();

            entry.getChannels().forEach(channel -> {
                if (channels.contains(channel)) {
                    IntStream.range(0, channels.size()).forEach(idx -> {
                        if (channel.equals(channels.get(idx))) {
                            var address = device.getStartAddress() + idx;
                            verifyUniqueAddressDefinition(configuration.getName(), deviceName, channel, address);
                            functionLookup.put(address, buildFunction(entry, deviceNumber));
                        }
                    });
                }
            });
        }));
    }

    private static Device lookupDevice(Map<String, Device> deviceMap, String deviceName) {
        if (!deviceMap.containsKey(deviceName)) {
            throw new ConfigurationException("Cannot find device '" + deviceName + "'");
        }

        return deviceMap.get(deviceName);
    }

    private void verifyUniqueAddressDefinition(String sceneName, String deviceName, Channel channel, int address) {
        if (functionLookup.containsKey(address)) {
            throw new ConfigurationException("Duplicate assignment of channel '" + channel + "' on device '"
                    + deviceName + "' for scene '" + sceneName + "'");
        }
    }

    private static Function buildFunction(ValueConfiguration entry, int deviceNumber) {
        return switch (entry.getType()) {
            case STATIC -> new FunctionStatic(entry);
            case SINE -> new FunctionSine(entry, deviceNumber);
            case SQUARE -> new FunctionSquare(entry, deviceNumber);
            case TRIANGLE, RAMP_UP, RAMP_DOWN -> new FunctionSaw(entry, deviceNumber);
        };
    }

    public Dmx getDmx(DmxContext context) {
        var channels = functionLookup.entrySet().stream().collect(Collectors.toMap(
                Map.Entry::getKey,
                e -> e.getValue().getChannelValues(context)));
        return new Dmx(channels);
    }

}
