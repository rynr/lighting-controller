package org.rjung.service.lighting.show.internal.function;

import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.rjung.service.lighting.configuration.ValueConfiguration;

@EqualsAndHashCode(callSuper = true)
@ToString
public class FunctionSquare extends AbstractMinMaxFunction {

    public FunctionSquare(ValueConfiguration configuration, Integer deviceNr) {
        super(configuration, deviceNr);
    }

    @Override
    protected byte function(double input) {
        return (byte) (input < 0.5 ? min : max);
    }

}
