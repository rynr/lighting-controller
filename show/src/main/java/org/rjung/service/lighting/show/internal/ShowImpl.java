package org.rjung.service.lighting.show.internal;

import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.rjung.service.lighting.configuration.DeviceConfiguration;
import org.rjung.service.lighting.configuration.FixtureConfiguration;
import org.rjung.service.lighting.configuration.SceneConfiguration;
import org.rjung.service.lighting.configuration.ShowConfiguration;
import org.rjung.service.lighting.shared.Dmx;
import org.rjung.service.lighting.show.Show;

import java.time.Instant;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@EqualsAndHashCode
@ToString
public class ShowImpl implements Show {

    private final List<Section> sections;
    private final AtomicInteger currentSection;
    private boolean effectOn;

    public ShowImpl(ShowConfiguration configuration) {
        currentSection = new AtomicInteger(0);
        effectOn = false;
        var fixtures = configuration.getFixtures().stream()
                .collect(Collectors.toMap(FixtureConfiguration::getName, Fixture::new));
        var devices = configuration.getDevices().stream()
                .collect(Collectors.toMap(DeviceConfiguration::getName, device -> new Device(device, fixtures)));
        var scenes = configuration.getScenes().stream()
                .collect(Collectors.toMap(SceneConfiguration::getName, scene -> new Scene(scene, devices)));
        sections = configuration.getSections().stream().map(section -> new Section(section, scenes)).toList();
    }

    @Override
    public String getSection() {
        int currentSectionIndex = currentSection.get();
        return sections.size() > currentSectionIndex ? sections.get(currentSectionIndex).getName() : null;
    }

    @Override
    public void prevSection() {
        currentSection.getAndUpdate(section -> section > 0 ? section - 1 : section);
    }

    @Override
    public void nextSection() {
        currentSection.getAndUpdate(section -> section < sections.size() - 1 ? section + 1 : section);
    }

    @Override
    public void setEffect(boolean effectEnabled) {
        effectOn = effectEnabled;
    }

    @Override
    public Dmx getDmx(Instant instant) {
        return sections.get(currentSection.get()).getDmx(instant, this.effectOn);
    }
}
