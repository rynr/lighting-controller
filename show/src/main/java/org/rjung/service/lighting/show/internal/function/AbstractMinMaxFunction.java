package org.rjung.service.lighting.show.internal.function;

import lombok.EqualsAndHashCode;
import org.rjung.service.lighting.configuration.Channel;
import org.rjung.service.lighting.configuration.ValueConfiguration;
import org.rjung.service.lighting.shared.ConfigurationException;
import org.rjung.service.lighting.shared.DmxContext;
import org.rjung.service.lighting.show.internal.Function;

import java.util.Set;

@EqualsAndHashCode
public abstract class AbstractMinMaxFunction implements Function {

    public static final long NANOS_PER_SECOND = 1000000000L;
    public static final long NANOS_PER_MINUTE = 60 * NANOS_PER_SECOND;

    protected final Set<Channel> channel;
    protected final double min;
    protected final double max;
    protected final double rate;
    protected final short offset;

    protected AbstractMinMaxFunction(ValueConfiguration configuration, int deviceNr) {
        // Store channel
        channel = configuration.getChannels();
        rate = configuration.getRate() != null ? configuration.getRate() : 1;
        int phaseOffset = deviceNr * configuration.getPhase();
        if (configuration.getOffset() != null) {
            offset = (short) ((configuration.getOffset() + phaseOffset) % 360);
        } else {
            offset = (short) (phaseOffset % 360);
        }

        // retrieve min/max
        min = configuration.getMin() == null ? 0D : configuration.getMin().doubleValue();
        max = configuration.getValue() == null ? 255.999 : configuration.getValue().doubleValue() + 0.999;

        if (min < 0 || max < 0) {
            throw new ConfigurationException("Values need to be >= 0");
        }
        if (min >= 256 || max >= 256) {
            throw new ConfigurationException("Values need to be <= 255");
        }
        if (rate < 0.001 || rate > 4) {
            throw new ConfigurationException("Rate needs to be within 0.001 and 4");
        }
    }

    @Override
    public Byte getChannelValues(DmxContext context) {
        if (context.bpm() <= 0) {
            return (byte) max;
        }

        var timestamp = context.instant();
        long nanosPerBeat = (long) (NANOS_PER_MINUTE / context.bpm() / rate);
        long nanoOffsetPerBeat = (long) ((double) nanosPerBeat / 360) * offset;
        var nanoSinceBeat = (timestamp.getEpochSecond() * NANOS_PER_SECOND + timestamp.getNano() + nanoOffsetPerBeat) % nanosPerBeat;
        double normedInput = (double) nanoSinceBeat / nanosPerBeat;
        return function(normedInput);
    }

    /**
     * Implement the function to return a byte value with the linear input from 0 to 1.
     *
     * @param input value from 0 to 1
     * @return DMX value (0-255) depending on function
     */
    protected abstract byte function(double input);

}
