package org.rjung.service.lighting.runner;

import ch.bildspur.artnet.ArtNetClient;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import jakarta.annotation.PreDestroy;
import org.rjung.service.lighting.configuration.LightingControlConfiguration;
import org.rjung.service.lighting.configuration.MidiCommandConfiguration;
import org.rjung.service.lighting.show.Show;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;

import javax.sound.midi.*;
import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.util.*;

/**
 * This application is quite a hack. The main effort went into the configuration and show. Thus, there's no testing on
 * this code. There could follow a better implementation, but for now it works (for me).
 */
public class LightingControlApplication implements ApplicationRunner, Receiver {
    private static final Logger LOG = LoggerFactory.getLogger(LightingControlApplication.class);
    private final Set<ArtNetDestination> destinations;
    // Using byte[] as key does not work. Using new String(byte[]) instead.
    private final Map<String, MidiCommandConfiguration.Command> midiMapping;
    private Show show;
    private ArtNetClient artnet;
    private Timer timer;

    public LightingControlApplication() {
        this.destinations = new HashSet<>();
        this.midiMapping = new HashMap<>();
    }

    public static void main(String[] args) {
        SpringApplication.run(LightingControlApplication.class, args);
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        show = loadShowFromArguments(args);
        setupMidiCheckTimer();
        artnet = new ArtNetClient();
        artnet.start();
        LOG.info(show.getSection());
    }

    private Show loadShowFromArguments(ApplicationArguments arguments) throws IOException {
        var showSource = "show.yaml";
        if (arguments.containsOption("show")) {
            var shows = arguments.getOptionValues("show");
            if (shows.size() == 1) {
                showSource = shows.get(0);
            }
        }
        LOG.info("Opening show {}", showSource);
        var objectMapper = new ObjectMapper(new YAMLFactory());
        var showConfiguration = objectMapper.readValue(new File(showSource), LightingControlConfiguration.class);
        for (var dest : showConfiguration.getArtNet()) {
            destinations.add(new ArtNetDestination(InetAddress.getByName(dest.getHost()), dest.getUniverse(), dest.getSubnet()));
        }
        for (var midiCommand : showConfiguration.getMidiCommands()) {
            var buffer = ByteBuffer.allocate(midiCommand.getMidi().size());
            midiCommand.getMidi().forEach(action -> buffer.put(action.byteValue()));
            midiMapping.put(new String(buffer.array()), midiCommand.getCommand());
        }
        setupTimer(showConfiguration.getFrameRate());
        return Show.build(showConfiguration.getShow());
    }

    private void setupTimer(LightingControlConfiguration.FrameRate frameRate) {
        var task = new TimerTask() {
            public void run() {
                send(show.getDmx().getBytes());
            }
        };

        timer = new Timer("ART-NET");

        timer.scheduleAtFixedRate(task, 1009L, switch (frameRate) {
            case _1_HZ -> 1000L;
            case _5_HZ -> 200L;
            case _10_HZ -> 100L;
            case _20_HZ -> 50L;
            case _25_HZ -> 40L;
            case _40_HZ -> 25L;
            case _43_HZ -> 23L;
        });
    }

    private void setupMidiCheckTimer() {
        var receiver = this;
        var task = new TimerTask() {

            public void run() {
                for (var info : MidiSystem.getMidiDeviceInfo()) {
                    try {
                        var device = MidiSystem.getMidiDevice(info);
                        if ("com.sun.media.sound.MidiInDevice".equals(device.getClass().getName())
                                && !device.isOpen()) {
                            for (Transmitter transmitter : device.getTransmitters()) {
                                connectTransmitter(transmitter);
                            }

                            Transmitter trans = device.getTransmitter();
                            connectTransmitter(trans);

                            device.open();
                        }
                    } catch (MidiUnavailableException ex) {
                        LOG.debug("Could not open midi device {}", info);
                    }
                }
            }

            private void connectTransmitter(Transmitter transmitter) {
                transmitter.setReceiver(receiver);
                LOG.info("Connected to midi device");
            }
        };
        timer = new Timer("MIDI");
        timer.scheduleAtFixedRate(task, 997L, 1999L);
    }

    @Override
    public void send(MidiMessage message, long timeStamp) {
        var byteMessage = new String(message.getMessage());
        if (midiMapping.containsKey(byteMessage)) {
            var command = midiMapping.get(byteMessage);
            if (command == MidiCommandConfiguration.Command.NEXT) {
                show.nextSection();
            } else if (command == MidiCommandConfiguration.Command.PREVIOUS) {
                show.prevSection();
            }
        }
        LOG.info(show.getSection());
    }

    private void send(byte[] bytes) {
        destinations.forEach(dest -> artnet.unicastDmx(dest.address, dest.subnet(), dest.universe(), bytes));
    }

    @Override
    public void close() {
        LOG.info("Received close for midi connection");
    }

    @PreDestroy
    public void shutdown() {
        LOG.info("Shutting down");
        timer.cancel();
    }

    private record ArtNetDestination(InetAddress address, int universe, int subnet) {
    }
}
