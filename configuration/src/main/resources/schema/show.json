{
  "$schema": "https://json-schema.org/draft/2020-12/schema",
  "$id": "https://rynr.gitlab.io/lighting-controller/show.json",
  "title": "Show",
  "description": "Lighting-Control configuration",
  "type": "object",
  "properties": {
    "fixtures": {
      "description": "Configuration of all fixtures available to be used.",
      "type": "array",
      "items": {
        "type": "object",
        "properties": {
          "name": {
            "description": "Unique identifier of the fixture to be referenced by a device.",
            "type": "string"
          },
          "channels": {
            "description": "Type of a Channel.",
            "type": "array",
            "items": {
              "$ref": "channel.json"
            },
            "minItems": 1
          }
        },
        "required": [
          "name",
          "channels"
        ]
      },
      "minItems": 1,
      "uniqueItems": true
    },
    "devices": {
      "description": "Configuration of all devices available to be used.",
      "type": "array",
      "items": {
        "type": "object",
        "properties": {
          "name": {
            "description": "Unique identifier of the device to be referenced by a group or assignment.",
            "type": "string"
          },
          "fixture": {
            "description": "Reference to a name of a defined fixture.",
            "type": "string"
          },
          "start-address": {
            "description": "The start address of the device (0-511)",
            "type": "integer",
            "minimum": 0,
            "maximum": 511
          }
        },
        "required": [
          "name",
          "fixture",
          "start-address"
        ]
      },
      "minItems": 1,
      "uniqueItems": true
    },
    "scenes": {
      "description": "Configuration of scenes available to be used in any section.",
      "type": "array",
      "items": {
        "type": "object",
        "properties": {
          "name": {
            "description": "Unique identifier of the scene to be referenced by a section.",
            "type": "string"
          },
          "values": {
            "type": "array",
            "items": {
              "type": "object",
              "properties": {
                "devices": {
                  "description": "List of channel assignments.",
                  "type": "array",
                  "items": {
                    "type": "string"
                  },
                  "uniqueItems": true
                },
                "channels": {
                  "type": "array",
                  "description": "Set of all channels to have the value assigned.",
                  "items": {
                    "$ref": "channel.json"
                  },
                  "uniqueItems": true
                },
                "value": {
                  "type": "integer",
                  "description": "The value to be assigned. The default value is 0. If there's ranges defined, this property is ignored.",
                  "minimum": 0,
                  "maximum": 255,
                  "default": 255
                },
                "min": {
                  "type": "integer",
                  "description": "The value to be assigned. The default value is 0. If there's ranges defined, this property is ignored.",
                  "minimum": 0,
                  "maximum": 255,
                  "default": 0
                },
                "type": {
                  "type": "string",
                  "enum": [
                    "static",
                    "sine",
                    "square",
                    "triangle",
                    "ramp-up",
                    "ramp-down"
                  ],
                  "default": "static"
                },
                "rate": {
                  "type": "number",
                  "description": "The rate of the function. The rate 2 runs the function with double speed. The default is 1.",
                  "minimum": 0.001,
                  "maximum": 4,
                  "default": 1
                },
                "offset": {
                  "type": "integer",
                  "description": "The global offset (0-359) of the function. The default is 0.",
                  "minimum": 0,
                  "maximum": 359,
                  "default": 0
                },
                "phase": {
                  "type": "integer",
                  "description": "The offset (0-359) of any device after the previous. The default is 0.",
                  "minimum": 0,
                  "maximum": 359,
                  "default": 0
                }
              }
            }
          }
        },
        "required": [
          "name"
        ]
      },
      "uniqueItems": true
    },
    "sections": {
      "description": "Sections to be shown in the order of their definition.",
      "type": "array",
      "items": {
        "type": "object",
        "properties": {
          "name": {
            "description": "Name of the section which describes the current section.",
            "type": "string"
          },
          "bpm": {
            "description": "The speed of the section in bpm (Beats per Minute). The default is 120 bpm.",
            "type": "integer",
            "minimum": 0,
            "maximum": 555,
            "default": 120
          },
          "scenes": {
            "description": "List of references to the scenes of the section.",
            "type": "array",
            "items": {
              "type": "string"
            },
            "minItems": 1,
            "uniqueItems": true
          },
          "effect": {
            "description": "List of references to scenes which replace the scenes for special effect.",
            "type": "array",
            "items": {
              "type": "string"
            },
            "uniqueItems": true
          }
        }
      },
      "minItems": 1
    }
  },
  "required": [
    "sections"
  ]
}
