package org.rjung.service.lighting.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.isA;

class YamlDeserializationTest {
    private ObjectMapper objectMapper;

    @BeforeEach
    void setup() {
        objectMapper = new ObjectMapper(new YAMLFactory());
    }

    @Test
    void shouldBeAbleToReadShowFromYaml() throws IOException {
        InputStream yamlConfiguration = YamlDeserializationTest.class.getClassLoader().getResourceAsStream("example-show.yaml");

        var show = objectMapper.readValue(yamlConfiguration, LightingControlConfiguration.class);

        assertThat(show, isA(LightingControlConfiguration.class));
    }

}
