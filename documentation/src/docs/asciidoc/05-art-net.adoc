= Show / Art-Net configuration

// video::TBD[youtube]

Somehow the devices need to be controlled.
This project uses link:index.html#art-net[ArtNet]. +
So we need to configure where to send the data to.

The simplest configuration is to just set a host (`127.0.0.1` as an example).
The universe will default to `0`.

[source,yaml]
----
art-net:
 - host: 127.0.0.1
----

But you can define multiple of them and add an `universe` and `subnet` configuration, they all receive the same data (for now).

Last, we need to control the show, this is configured via link:06-midi[Midi].