= Show / Run LightingControl

// video::TBD[youtube]

Here we are, we have configured link:01-fixture.html[Fixtures], used the to configure link:02-device.html[Devices].
Then we created link:03-scene[Scenes] which are combined to link:04-section[Sections]. +
We also configured link:05-art-net[ArtNet] and a way to control the show via link:06-midi[MIDI].

To run everything we need two more things. +
First we need a Java Runtime Environment (version 17) which can be retrieved from link:https://www.oracle.com/java/technologies/downloads/[Oracle].
Then we need a jar-file (not the pom-file) version of the LightingControl runner (`org/rjung/service/lighting-control-runner`), which can be downloaded from link:https://gitlab.com/rynr/lighting-controller/-/packages[gitlab] (ideally pic the latest version).

The Java Runtime Environment comes with an executable `java`.
That one is called with the following parameters:

Expecting you have stored your configuration with the name `your-show.yaml`, the command to run would be like:

[source,shell]
----
java -jar lighting-control-runner.jar --show=your-show.yaml
----

Once started, you should see some output like:

[source,shell]
----
  _   _ ___ _ _ ___ _ _ _ ___    ___ ___ _ _ ___ ___ ___ _
  |__ | |_, |-|  |  | |\| |_, -- |__ [_] |\|  |  |-< [_] |__
20..-..-..T..:..:.....+..:00  INFO ..... --- [  main] o.r.s.l.r.LightingControlApplication     : Starting LightingControlApplication using Java 17 (lighting-control-runner.jar
20..-..-..T..:..:.....+..:00  INFO ..... --- [  main] o.r.s.l.r.LightingControlApplication     : No active profile set, falling back to 1 default profile: "default"
20..-..-..T..:..:.....+..:00  INFO ..... --- [  main] o.r.s.l.r.LightingControlApplication     : Started LightingControlApplication in 0.397 seconds (process running for 0.688)
20..-..-..T..:..:.....+..:00  INFO ..... --- [  main] o.r.s.l.r.LightingControlApplication     : Opening show your-show.yaml
20..-..-..T..:..:.....+..:00  INFO ..... --- [  main] o.r.s.l.r.LightingControlApplication     : Could not open midi device Gervill
20..-..-..T..:..:.....+..:00  INFO ..... --- [  main] o.r.s.l.r.LightingControlApplication     : Connected to midi device Real Time Sequencer
20..-..-..T..:..:.....+..:00  INFO ..... --- [  main] java.lang.Class                          : Art-Net server started at: 0.0.0.0:6454
----

You're good to go, your application is running.