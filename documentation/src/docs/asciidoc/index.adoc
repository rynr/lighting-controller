:projectdir: ../../../..

= Lighting-Control
:toc:

include::{projectdir}/README.adoc[tag=vision]

image:https://sonarcloud.io/api/project_badges/measure?project=rynr_lighting-controller&metric=alert_status[link="https://sonarcloud.io/summary/new_code?id=rynr_lighting-controller",title="Quality Gate Status"]
image:https://gitlab.com/rynr/lighting-controller/badges/main/pipeline.svg[link="https://gitlab.com/rynr/lighting-controller/-/commits/main",title="Pipeline Status"]
image:https://sonarcloud.io/api/project_badges/measure?project=rynr_lighting-controller&metric=coverage[link="https://sonarcloud.io/summary/new_code?id=rynr_lighting-controller",title=Coverage"]
image:https://gitlab.com/rynr/lighting-controller/-/badges/release.svg[link="https://gitlab.com/rynr/lighting-controller/-/releases",title="Latest Release"]

== Concept

Lighting-Control is made for a band wanting to control a small light show without the need to learn all configuration and function of a full-featured light-controller.

The Lighting-Control application does not have any user-interface.
It is controlled via a MIDI-Controller attached to the computer it runs on.
It uses Art-Net to send lighting information to the devices.
Lighting-Control needs to know which devices it is controlling and which data to send to the devices.
This is configured in a YAML-file (YAML stands for
`YAML Ain't Markup Language`), it defines a text-structure the configuration is stored in.

CAUTION: This project is not stable yet.
Minor updates can have the effect to change configuration properties completely.

== Tutorial

You can also take a look at the link:tutorial.html[Tutorial] which builds a complete show.

== Configuration

The configuration uses the `YAML` syntax.
If you are not familiar with `YAML`, try to understand that first.
Maybe a
link:https://quickref.me/yaml[cheat sheet] already explains enough.

=== Lighting-Control

The project has a configuration definition for its runner (defined by this `Lighting-Control` configuration, but it may (for other projects) only include the link:#show[Show] configuration.

[source,yaml]
----
---
name: Example Show
art-net:                # see art-net
midi-commands:          # see midi-commands
show:                   # see show
----

[%autowidth.stretch]
|===
|Property |Description

|`name`
|A name for the configuration (just to identify it)

|`art-net`
|Configuration on the connection to the devices.

|`midi-commands`
|Configuration for the commands via Midi.
|===

[#art-net]
=== Art-Net

This Version 0 only supports unique universes, but you can add as many as you like.
An art-net node hast as least a host defined.
The universe and subnet default to the values 0.

[source,yaml]
----
---
art-net:
 - host: 127.0.0.1
 - host: 10.11.12.13
   universe: 1
   subnet: 2
----

[%autowidth.stretch]
|===
|Property |Description

|`host`
|Hostname or IP address of the art-net node.

|`universe`
|The Universe of the node (defaults to `0`).

|`subnet`
|The Subnet of the node (defaults to `0`).
|===

=== Midi Command

You should (and currently can only) assign two Midi commands.
Without configuring a midi command, there will not happen anything.
This command tells the application, which midi command will trigger the next or previous section of the show.

This is the probably most used configuration setting a program change 1 to trigger the change to previous section and a program change 2 to the next section.

[source,yaml]
----
---
midi-commands:
  - midi:
      - 192
      - 1
    command: Previous
  - midi:
      - 192
      - 2
    command: Next
----

[%autowidth.stretch]
|===
|Property |Description

|`midi`
|Byte combination of the Midi command. Program Change is the first byte to be `192`, the second reflects the program.

|`command`
|The command to be mapped to the midi command. There are only `Previous` and `Next`.
|===

[#show]
=== Show

The main entity defined is the `show`.It has the following properties:

[%autowidth.stretch]
|===
|Property |Description

|`fixtures`
|A list of link:#fixture[fixtures] that can be used to define the link:#device[devices].

|`devices`
|A list of link:#device[devices] that can be used in link:#scene[scenes] to assign values to their link:#channel[channels].

|`scenes`
|A list of link:#scene[scenes] that can be used in link:#section[sections] to define the link:#channel[channels] to be sent to a link:#device[device].

|`sections`
|An ordered list of the different link:#section[sections] which link:#scene[scenes] to be sent.
|===

A show configuration looks like this:

[source,yaml]
----
---
show:
  fixtures:       # see fixtures
  devices:        # see devices
  scenes:         # see scenes
  sections:       # see sections
----

[#fixture]
==== Fixture

The `fixture` defines one or more lists of channels a link:#device[device] has defined.Like this, multiple devices with the same channels do not need to have all channels redefined.

A `fixture` has the following properties defined:

[%autowidth.stretch]
|===
|Property |Description

|`name`
|Identifier of this `fixture`. It needs to be unique within all other `fixtures`.

|`channels`
|List of channels, see link:#channel[Channels]
|===

Fixture configurations look like these:

[source,yaml]
----
  fixtures:
    - name: rgb
      channels:
        - Red
        - Green
        - Blue
    - name: 2head-rgb
      channels:
        - Red
        - Green
        - Blue
        - Red
        - Green
        - Blue
----

[#channel]
==== Channel

Channels can have different types.

The following types are available (Others will be available in link:https://gitlab.com/rynr/lighting-controller/-/blob/main/ROADMAP.adoc[later versions]):

[%autowidth.stretch]
|===
|Channel Type |Description

|Master
|A Master channel. Setting the master will not affect any other channels.

|Red
|Generic channel for the color Red.

|Green
|Generic channel for the color Green.

|Blue
|Generic channel for the color Blue.

|White
|Generic channel for the color White.

|Amber
|Generic channel for the color Amber.

|UV
|Generic channel for the color Ultraviolet.

|Strobe
|Channel for any Strobe/Shutter/Flash function.

|Pan
|Channel for Pan-Axis of moving devices.

|Tilt
|Channel for Tilt-Axis of moving devices.

|Show/Program
|Channel selecting programs provided by the device.

|Color-Wheel
|Channel selecting a given range of colors.

|Gobo-Wheel
|Channel for selecting gobos.

|Moving-Speed
|Channel to define the moving speed of any pan/tilt/program.

|Fog
|Channel to control any Fog machine.

|Misc1
|Channel(1) for any additional control.

|Misc2
|Channel(2) for any additional control.

|Misc3
|Channel(3) for any additional control.
|===

[#device]
==== Devices

Devices are configured by setting a link:#fixture[fixture] and their start address.
There's no check for non overlapping fixtures.
The fixture is identified with the name given to the fixture.

[%autowidth.stretch]
|===
|Property |Description

|`name`
|Unique identifier of the device to be referenced by a scene.

|`fixture`
|Reference to a name of a defined fixture.

|`start-address`
|The start address of the device (0-511)
|===

A Segment configurations look like these:

[source,yaml]
----
  devices:
    - name: par
      fixture: rgb
      start-address: 0
    - name: twin-par
      fixture: 2head-rgb
      start-address: 4
----

[#scene]
==== Scene

The Scene assigns channel values to the different link:#device[devices]. A Scene may assign different values to devices.

[%autowidth.stretch]
|===
|Property |Description

|`name`
|Unique identifier of the scene to be referenced by a section.

|`devices`
|List of all `device` name to get an assignment.

|`sources`
|The sources define a list of channels and values to be assigned.
|===

[source,yaml]
----
  scenes:
    - name: white
      values:
        - devices:
            - par
          channels:
            - Red
            - Green
            - Blue
          value: 255
    - name: blue
      values:
        - devices:
            - par
          channels:
            - Red
            - Green
          value: 0
        - devices:
            - par
          channels:
            - Blue
          value: 255
----

[#values]
==== Values

Value / Function assignments to devices.

[%autowidth.stretch]
|===
|Property |Description

|`devices`
|List of devices. The order makes a difference when used with `phase`.

|`channels`
|List of channels.

|`value`
|The DMX value of the defined channels (0-255).

|`min`
|The DMX minimum value of the defined channels (0-`<value>`) if a type (function) is given (defaults to `0`).

|`type`
|The default `type` (function) is `Static`, which means a static value (checkout link:#type[type]).

|`rate`
|The rate of the function. The rate 2 runs the function with double speed. The default is 1.

|`offset`
|The offset (0-359) of the function. The default is 0.

|`phase`
|The phase (0-359) between the devices.
|===

[source,yaml]
----
        - devices:
            - par
          channels:
            - Red
            - Green
          value: 0
        - devices:
            - par
          channels:
            - Blue
          value: 255
        - devices:
            - par
            - twin-par
          channels:
            - Master
-         value: 255
          min: 127
          type: Sine
          rate: 0.0625
          phase: 180
----

[#type]
==== Type

[%autowidth.stretch]
|===
|Property |Description

|`Static`
|No function. The property from `value` is chosen, if none is defined the value is the maximum (255).

|`Sine`
|The sinus function oscillates between the lower and upper bound of the range. If only a `value` is given, the oscillation is between 0 and the `value`.

|`Saw`
|The saw function increases the value from the lower to the upper bound of the range. If only a `value` is given, the oscillation is between 0 and the `value`.

|`Square`
|The saw function alternates between the lower and upper bound of the range. If only a `value` is given, the oscillation is between 0 and the `value`.
|===

[#section]
==== Section

Sections define which scenes are run one after the other.Each Scene has a name and a list of scenes to be activated.

[%autowidth.stretch]
|===
|Property |Description

|`name`
|Name defining the Section.

|`scenes`
|List of all link:#scene[Scenes] to be active during the Section. No Scenes mean all channels are set to `0`.

|`bpm`
|The beats per minutes (bpm) of the section. Functions are oscillating in the given speed. The default is 120.
|===

[source,yaml]
----
  sections:
    - name: Black-out
    - name: Red
      scenes:
        - red
----

== Run Lighting-Control

=== Requirements

* Any computer which has a java runtime
* MIDI interface to receive MIDI messages
* Network access to send Art-Net messages
* Additional Hardware:
** Midi-Controller (like the FCB 1010) to send messages to Lighting-Control
** Lighting Hardware which does understand Art-Net

=== Run Command

You need to build or link:https://gitlab.com/rynr/lighting-controller/-/releases[download] the `lighting-control.jar`.
Having a java runtime environment installed, you can execute the following command:

[source,console]
----
$ java -jar lighting-control.jar --show=show.yaml
----

[%autowidth.stretch]
|===
|Part |Explanation

|`java`
|Call of the java runtime environment.

|`-jar`
|Parameter to tell `java` to execute the next parameter as java archive (`jar`).

|`lighting-controller.jar`
|The compiled application, the name can be different. To call it like this, it needs to be in the same directory.

|`--show=show.yaml`
|Defines which configuration to run. If your configuration s not in a file `show.yaml`, change the name.
|===

== Developers

If you are interested to use parts of this project, checkout the documentation for link:developers.html[Developers].