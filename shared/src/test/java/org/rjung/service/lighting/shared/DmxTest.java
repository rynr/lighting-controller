package org.rjung.service.lighting.shared;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

class DmxTest {

    @Test
    void shouldBeEmptyByDefault() {
        var sut = new Dmx();

        assertThat(sut.getBytes(), is(new byte[0]));
    }

    @Test
    void shouldProvideByteData() {
        var data = new byte[]{0, 1, 2, 3, 4, 5, 6};

        var sut = new Dmx(data);

        assertThat(sut.getBytes(), is(data));
    }

    @Test
    void shouldSetByteAtOffset() {
        var sut = new Dmx(new byte[]{0, 1, 2, 3, 4, 5});

        sut.setByte((byte) 13, 3);

        assertThat(sut.getBytes(), is(new byte[]{0, 1, 2, 13, 4, 5}));
    }

    @Test
    void shouldSetInt8AtOffset() {
        var sut = new Dmx(new byte[]{0, 1, 2, 3, 4, 5});

        sut.setInt8(13, 3);

        assertThat(sut.getBytes(), is(new byte[]{0, 1, 2, 13, 4, 5}));
    }

    @Test
    void shouldSetStrippedInt8AtOffset() {
        var sut = new Dmx(new byte[]{0, 1, 2, 3, 4, 5});

        sut.setInt8(269, 3); // 256 + 13 = 269

        assertThat(sut.getBytes(), is(new byte[]{0, 1, 2, 13, 4, 5}));
    }

    @Test
    void shouldJoinTwoDmx() {
        var dmx1 = new Dmx();
        var dmx2 = new Dmx();

        dmx1.setInt8(1, 1);
        dmx2.setInt8(2, 2);

        var joinedDmx = Dmx.join(dmx1, dmx2);

        assertThat(joinedDmx.getBytes()[1], is((byte) 1));
        assertThat(joinedDmx.getBytes()[2], is((byte) 2));
    }

    @Test
    void shouldUseFirstDmxValueOnConflict() {
        var dmx1 = new Dmx();
        var dmx2 = new Dmx();

        dmx1.setInt8(1, 1);
        dmx2.setInt8(2, 1);

        var joinedDmx1 = Dmx.join(dmx1, dmx2);
        assertThat(joinedDmx1.getBytes()[1], is((byte) 2));

        var joinedDmx2 = Dmx.join(dmx2, dmx1);
        assertThat(joinedDmx2.getBytes()[1], is((byte) 2));
    }

}