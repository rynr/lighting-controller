package org.rjung.service.lighting.shared;

import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

class ConfigurationExceptionTest {

    @Test
    void shouldHaveMessageFromConstructor() {
        var message = UUID.randomUUID().toString();

        var sut = new ConfigurationException(message);

        assertThat(sut.getMessage(), is(message));
    }

}