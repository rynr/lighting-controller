package org.rjung.service.lighting.shared;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

@EqualsAndHashCode
@ToString
public class Dmx {

    private final Map<Integer, Byte> channels;

    public Dmx() {
        channels = new HashMap<>();
    }

    public Dmx(byte[] data) {
        this();

        for (int i = 0; i < data.length; i++) {
            channels.put(i, data[i]);
        }
    }

    public Dmx(Map<Integer, Byte> channels) {
        this.channels = channels;
    }

    public static Dmx join(Dmx... dmxes) {
        var channels = Arrays.stream(dmxes)
                .flatMap(dmx -> dmx.channels.entrySet().stream())
                .collect(Collectors.toMap(Map.Entry::getKey,
                        Map.Entry::getValue,
                        (a, b) -> a > b ? a : b));

        return new Dmx(channels);
    }

    public byte[] getBytes() {
        int size = channels.keySet().stream().max(Integer::compareTo).map(i -> i + 1).orElse(0);
        var result = new byte[size];
        for (int i = 0; i < size; i++) {
            result[i] = channels.getOrDefault(i, (byte) 0);
        }
        return result;
    }

    public final void setByte(byte val, int offset) {
        channels.put(offset, val);
    }

    public final void setInt8(int val, int offset) {
        channels.put(offset, (byte) (val & 0xff));
    }

}
