package org.rjung.service.lighting.shared;

import java.time.Instant;

public record DmxContext(Short bpm, Instant instant) {
}
